# encoding: UTF-8

get '/api/v1/todos' do
  format_response(Item.all, request.accept)
end

get '/api/v1/todos/:id' do
  item ||= Item.get(params[:id]) || halt(404)
  format_response(item, request.accept)
end

post '/api/v1/todos' do
	body = JSON.parse request.body.read
	item = Item.create(
		text: body['text'],
		complete: body['complete']
		)
	status 201
	format_response(item, request.accept)
end

put 'api/v1/todos/:id' do
	body = JSON.parse request.body.read
	item ||= Item.get(params[:id]) || halt(404)
	halp 500 unless item.update(
		text: body['text'],
		complete: body['complete']
		)
	format_response(item, request.accept)
end

delete '/api/v1/todos/:id' do 
	item ||= Item.get(params[:id]) || halt(404)
	halt 500 unless item.destroy
end
