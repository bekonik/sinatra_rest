# encoding: UTF-8
class Item
  include DataMapper::Resource

  property :id,        Serial
  property :text,      String
  property :complete,   Boolean
end 